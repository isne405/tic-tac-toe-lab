#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;

int BotDecision(int *[3],bool &);
int tossacion();
void showgrid(int[3][3]);
int winners(int *[3]);
void playersturn(int *[3]);
void main() {
	srand(time(0));
	bool isboardEmpty;
	int whoseturn;
	int winornot = 0;
	bool endgame = false;
	int gridsize = 3;
	int newGridsize = 3;
	int** grid = new int* [gridsize];
	for (int i = 0; i < gridsize; ++i) {
		grid[i] = new int[gridsize];
	}

	
	for(int i = 0 ; i < gridsize;i++){
		for (int j = 0; j < gridsize; j++) {
			grid[i][j] = 0;
			}
		}

	for(int i = 0 ; i < gridsize ; i++){
		for (int j = 0; j < gridsize; j++) {
			cout << grid[i][j] << " ";
			if (j == 2) {
				cout << endl;
			}
		}
	}

	
	whoseturn = tossacion();
	if (whoseturn == 0) {
		cout << "Bot go first" << "play as O" << endl;
		isboardEmpty = true;
	}else{
		cout << "You go first" << " Play as X" << endl;
		isboardEmpty = false;
	}
	cout << whoseturn <<  endl;

	while (endgame != true)
	{
		if (whoseturn == 0) {
			BotDecision(grid, isboardEmpty);
			playersturn(grid);
			winornot = winners(grid);
			if (winornot != 0) {
				if (winornot == 1) {
					cout << "Players wins!";
					endgame = true;
				}
				else if (winornot == 2) {
					cout << "You lose";
					endgame = true;
				}
			}
		}
	}

	cout << endl << "GG";
}


int tossacion() {
	return rand()% 2 ;
}

int BotDecision(int (&arr)[3][3],bool &boardempty) {
	int coorI;
	int coorJ;
	int method;
	if (boardempty) {
		//bot play first;
		coorI = rand() % 2;
		coorI = rand() % 2;
		if (coorI == 1) {
			coorI = 3;
		}
		if (coorJ == 1) {
			coorJ = 3;
		}
		arr[coorI][coorJ] = 2;
		boardempty = false;
	}
	else {
		//bot play second;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (arr[i][j] == 1 && ((arr[i][j] == arr[i+1][j]) && i == 1 ) && arr[0][j] == 0) {
					arr[0][j] = 2;
					break;
				}
				else if (arr[i][j] == 1 && ((arr[i][j] == arr[i + 1][j]) && i == 0) && arr[2][j] == 0) {
					arr[2][j] = 2;
					break;
				}
				else if (arr[i][j] == 1 && ((arr[i][j] == arr[i][j + 1]) && j == 1) && arr[i][0] == 0) {
					arr[i][0] = 2;
					break;
				}
				else if (arr[i][j] == 1 && ((arr[i][j] == arr[i][j + 1]) && j == 0) && arr[i][2] == 0) {
					arr[i][2] = 2;
					break;
				}
				else if (arr[i][j] == 1 && ((arr[i][j] == arr[i + 1][j + 1]) && i == 0 && j == 0) && arr[2][2] == 0) {
					arr[2][2] = 2;
					break;
				}
				else if (arr[i][j] == 1 && ((arr[i][j] == arr[i + 1][j + 1]) && i == 1 && j == 1) && arr[0][0] == 0) {
					arr[0][0] = 2;
					break;
				}
				else if (arr[i][j] == 1 && ((arr[i][j] == arr[i + 1][j - 1]) && i == 0 && j == 2) && arr[2][0] == 0) {
					arr[2][0] = 1;
					break;
				}
				else if (arr[i][j] == 1 && ((arr[i][j] == arr[i + 1][j - 1]) && i == 1 && j == 1) && arr[0][2] ==0) {
					arr[0][2] = 2;
					break;
				}
				
				if (arr[i][j] == 1 && ((arr[i][j] == arr[i + 2][j]) && i == 0) && arr[1][j] == 0) {
					arr[1][j] = 2;
					break;
				}
				else if (arr[i][j] == 1 && ((arr[i][j] == arr[i - 2][j]) && i == 2) && arr[0][j] == 0) {
					arr[0][j] = 2;
					break;
				}
				else if (arr[i][j] == 1 && ((arr[i][j] == arr[i][j + 2]) && j == 0) && arr[i][1] == 0) {
					arr[i][1] = 2;
					break;
				}
				else if (arr[i][j] == 1 && ((arr[i][j] == arr[i][j - 2]) && j == 0) && arr[i][0] == 0) {
					arr[i][0] = 2;
					break;
				}
				else if (arr[i][j] == 1 && ((arr[i][j] == arr[i + 2][j + 2]) && i == 0 && j == 0) && arr[1][1] == 0) {
					arr[1][1] = 2;
					break;
				}
				else if (arr[i][j] == 1 && ((arr[i][j] == arr[i - 2][j - 2]) && i == 2 && j == 2) && arr[1][1] == 0) {
					arr[1][1] = 2;
					break;
				}
				else if (arr[i][j] == 1 && ((arr[i][j] == arr[i + 2][j - 2]) && i == 0 && j == 2) && arr[1][1] == 0) {
					arr[1][1] = 2;
					break;
				}
				else if (arr[i][j] == 1 && ((arr[i][j] == arr[i - 2][j + 2]) && i == 2 && j == 0) && arr[1][1] == 0) {
					arr[1][1] = 2;
					break;
				}
				
				
				else if (((i == 0 || i == 2) && (j == 0 || j == 2)) && arr[i][j] == 1 && arr[1][1] == 0 && arr[1][1] == 0) {
					arr[1][1] = 2;
					break;
				}
				
				else if (arr[1][1] == 1) {
					if (arr[0][0] == 0) {
						arr[0][0] = 2;
						break;
					}
					else if (arr[0][2] == 0) {
						arr[0][2] = 2;
						break;
					}
					else if (arr[2][0] == 0) {
						arr[2][0] = 2;
						break;
					}
					else if (arr[2][2] == 0){
						arr[2][2] = 2;
						break;
					}
				}
				else if (i == 0 && (j == 0 || j == 2)) {
					if (arr[i][j] == 2) {
						if (arr[0][0] == 2) {
							if (arr[0][2] == 0) {
								arr[0][2] = 2;
								break;
							}
							else if(arr[2][0] == 0){
								arr[2][0] = 2;
								break;
							}
						}
						else if (arr[0][2] == 2) {
							if (arr[2][2] == 0) {
								arr[2][2] = 2;
								break;
							}
							else if(arr[0][0] == 0){
								arr[0][0] = 2;
								break;
							}
						}
					}
				}
				else if (i == 2 && (j == 0 || j == 2)){
					if (arr[i][j] == 2) {
						if (arr[2][0] == 2) {
							if (arr[0][0] == 0) {
								arr[0][0] = 2;
								break;
							}
							else if (arr[2][2] == 0) {
								arr[2][2] = 2;
								break;
							}
						}
						else if (arr[2][2] == 2) {
							if (arr[0][2] == 0) {
								arr[0][2] = 2;
								break;
							}
							else if (arr[2][0] == 0) {
								arr[2][0] = 2;
								break;
							}
						}
					}
				}				
				else if (arr[i][j] == 2 && (arr[i][j] == arr[i + 1][j])) {
					if (i == 0 && arr[2][j] == 0) {
						arr[2][j] = 2;
						break;
					}else if(i == 1 && arr[0][j] == 0){
						arr[0][j] = 2;
						break;
					}
				}
				else if (arr[i][j] == 2 && (arr[i][j] == arr[i][j+1])) {
					if (j == 0 && arr[i][2] == 0) {
						arr[i][2]= 2; 
						break;
					}
					else if (j == 1 && arr[i][0] == 0) {
						arr[i][0] = 2;
						break;
					}
				}
				else if (arr[i][j] == 2 && (arr[i][j] == arr[i + 1][j + 1])) {
					if (i == 0 && j == 0 && arr[2][2] == 0) {
						arr[i][j] = 2;
						break;
					}
					else if(i == 1 && j == 1 && arr[0][0] == 0){
						arr[0][0] = 2;
						break;
					}
				}
				else if (arr[i][j] == 2 && (arr[i][j] == arr[i + 1][j - 1])) {
					if (i == 0 && j == 2 && arr[2][0] == 0) {
						arr[2][0] = 2;
						break;
					}
					else if (i == 1 && j == 1 && arr[0][2] == 0) {
						arr[0][2] = 2;
						break;
					}
				}
				else {
					int randi = rand()%3;
					int randj = rand() % 3;
					arr[randi][randj] = 2;
					break;
				}
			}
		}
	}
}

void showgrid(int arr[3][3]) {
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			cout << "|";
			if (arr[i][j] == 0) {
				cout << " ";
			}
			else if (arr[i][j] == 1) {
				cout << "X";
			}
			else if (arr[i][j] == 2) {
				cout << "O";
			}
			if (i == 2) {
				cout << "|";
				cout << endl;
			}
		}
	}
}

int winners(int arr[3][3]) {
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			if (arr[i][j] == 1 && (arr[i][j] == arr[i + 1][j] == arr[i + 2][j])) {
				return 1;
			}
			else if (arr[i][j] == 1 && (arr[i][j] == arr[i][j + 1] == arr[i][j + 2])) {
				return 1;
			}
			else if (arr[i][j] == 1 && (arr[i][j] == arr[i + 1][j + 1] == arr[i + 2][j + 2])) {
				return 1;
			}
			else if (arr[i][j] == 1 && (arr[i][j] == arr[i + 1][j - 1] == arr[i + 2][j - 2])) {
				return 1;
			}
			else if (arr[i][j] == 2 && (arr[i][j] == arr[i + 1][j] == arr[i + 2][j])) {
				return 2;
			}
			else if (arr[i][j] == 1 && (arr[i][j] == arr[i][j + 1] == arr[i][j + 2])) {
				return 2;
			}
			else if (arr[i][j] == 1 && (arr[i][j] == arr[i + 1][j + 1] == arr[i + 2][j + 2])) {
				return 2;
			}
			else if (arr[i][j] == 1 && (arr[i][j] == arr[i + 1][j - 1] == arr[i + 2][j - 2])) {
				return 2;
			}
			else {
				return 0;
			}
		}
	}
}

void playersturn(int(&arr)[3][3]) {
	int r;
	int c;
	cout << "Please enter row : ";
	cin >> r;
	cout << "Please enter collumn : ";
	cin >> c;
	if (arr[r][c] == 0) {
		arr[r][c] = 1;
	}
	else {
		cout << "this coordinate is occupied please enter again" << endl;
		cout << "we will skip your turn";
	}
}